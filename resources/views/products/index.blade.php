@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
        <h3>
            Busqueda de Productos
            {{ Form::open(['route' => 'products.index', 'method' => 'GET', 'class' => 'form-inline pull-right']) }}
                <div class="form-group">
                    {{ Form::text('code', null, ['class' => 'form-control', 'placeholder' => 'Producto']) }}
                </div>
                <div class="form-group">
                    {{ Form::text('type', null, ['class' => 'form-control', 'placeholder' => 'Familia']) }}
                </div>
                <div class="form-group">
                    {{ Form::text('brand_id', null, ['class' => 'form-control', 'placeholder' => 'Proveedor']) }}
                </div>
                <div class="form-group">
                    {{ Form::text('created_at', null, ['class' => 'form-control', 'placeholder' => 'Fecha']) }}
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-default">Buscar
                        <span class="glyphicon glyphicon-search"></span>
                    </button>
                </div>
            {{ Form::close() }}
        </h3>
        </div>
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Productos
                <a class="btn btn-success float-right" href="{{route('product.create')}}">Nuevo</a>
                </div>

                <div class="card-body">
                <table class="table">
              <thead>
                <tr>
                  <th scope="col">#</th>
                  <th scope="col">Nombre</th>
                  <th scope="col">Código</th>
                  <th scope="col">Precio</th>
                  <th scope="col">Stock</th>
                  <th scope="col">Marca de Auto</th>
                  <th scope="col">Código Original</th>
                  <th scope="col">Categoría</th>
                  <th scope="col">Proveedor</th>
                  <th scope="col">Acciones</th>
                </tr>
              </thead>
              <tbody>
                @foreach($products as $product)
                  <tr>
                    <th scope="row">{{$product->id}}</th>
                    <td>{{$product->name}}</td>
                    <td>{{$product->code}}</td>
                    <td>{{$product->price}}</td>
                    <td>{{$product->amount}}</td>
                    <td>{{$product->car_brand}}</td>
                    <td>{{$product->original_code}}</td>
                    <td>{{$product->category->category}}</td>
                    <td>{{$product->brand->name}}</td>
                    <td>
                      <a href="{{ route('product.edit', $product->id)}}"><button type="button" class="btn btn-primary float-left">Editar</button></a>
                      <a href="{{ route('product.show', $product->id)}}"><button type="button" class="btn btn-primary float-left">Ver</button></a>                      
                      <form action="{{ route('product.destroy', $product)}}" method="POST" class="float-left">
                        @csrf
                        {{method_field('DELETE')}}
                        <button type="submit" class="btn btn-warning">Eliminar</button>
                      </form>
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
