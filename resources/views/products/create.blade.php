<!DOCTYPE html>
<html>

<head>
    <title>Nuevo</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css">
    <script type="text/javascript" charset="utf8" src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.2.min.js"></script>
</head>

<body>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
            @include('partials.error')
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-6">
                                <h2>{{ __('Registro') }}</h2>
                            </div>
                            <div class="col-md-6">
                                <div class="btn btn-lightr">
                                    <a href="{{ route('products.index') }}" class="btn btn-light">Regresar a Productos</a>
                                </div>
                            </div>
                        </div>
                    </div><br>

                    <div class="card-body">
                        <form method="POST" action="{{ route('product.store') }}" enctype="multipart/form-data">
                            {{@csrf_field()}}

                            <div class="form-group row">
                                <div class="col-md-4">
                                    <label for="name">{{ __('Nombre') }}</label>
                                    <input id="name" onkeypress="return soloLetras(event)" type="text" class="form-control" name="name" value="{{ old('name') }}"  autocomplete="name" autofocus>
                                </div>

                                <div class="col-md-4">
                                    <label for="code">{{ __('Código') }}</label>
                                    <input id="code" type="text" class="form-control" name="code" value="{{ old('code') }}"  autocomplete="code">
                                </div>
                                <div class="col-md-4">
                                    <label for="price">{{ __('Precio') }}</label>
                                    <input id="price" onkeypress="return filterFloat(event,this);" type="text" class="form-control" name="price" value="{{ old('price') }}"  autocomplete="price" autofocus>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-4">
                                    <label for="amount">{{ __('Cantidad') }}</label>
                                    <input id="amount" onkeypress="return soloNumeros(event)" type="number" class="form-control" name="amount" value="{{ old('amount') }}"  autocomplete="amount" autofocus>
                                </div>
                                <div class="col-md-4">
                                    <label for="car_brand">{{ __('Marca de Auto') }}</label>
                                    <input id="car_brand" onkeypress="return soloLetras(event)" type="text" class="form-control" name="car_brand" value="{{ old('car_brand') }}"  autocomplete="car_brand" autofocus>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-4">
                                    <label for="original_code">{{ __('Código Original') }}</label>
                                    <input id="original_code" type="text" class="form-control" name="original_code" value="{{ old('original_code') }}"  autocomplete="original_code" autofocus>
                                </div>
                                <div class="col-md-4">
                                    <label for="category">{{ __('Categoría') }}</label>
                                    <select name="category_id" id="category_id" class="form-control">
                                        @foreach($categories as $category)
                                        @if ($loop->first)
                                        <option value="{{$category->id}}" slected="selected">{{$category->category}}</option>
                                        @else
                                        <option value="{{$category->id}}">{{$category->category}}</option>
                                        @endif
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <label for="brand">{{ __('Proveedor') }}</label>
                                    <select name="brand_id" id="brand_id" class="form-control">
                                        @foreach($brands ?? '' as $brand)
                                        @if ($loop->first)
                                        <option value="{{$brand->id}}" slected="selected">{{$brand->name}}</option>
                                        @else
                                        <option value="{{$brand->id}}">{{$brand->name}}</option>
                                        @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="panel panel-footer">
                                <h2>Aplicaciones</h2>
                                <table class="table table-borderd">
                                    <thead>
                                        <tr>
                                            <th>Auto</th>
                                            <th>Cambio de Modelo</th>
                                            <th>Motorización</th>
                                            <th>Desde</th>
                                            <th>Hasta</th>
                                            <th><a class="addRow"><i class="glyphicon glyphicon-plus"></i></a></th>
                                        </tr>
                                    </thead>
                                    <tbody id="a">
                                        <tr>
                                            <td><input type="text" onkeypress="return soloLetras(event)" name="car[]" class="form-control car"></td>
                                            <td><input type="text" name="model_change[]" class="form-control model_change"></td>
                                            <td><input type="text" name="motorization[]" class="form-control motorization"></td>
                                            <td><input type="text" onkeypress="return soloNumeros(event)" name="since[]" class="form-control since"></td>
                                            <td><input type="text" onkeypress="return soloNumeros(event)" name="until[]" class="form-control until"></td>
                                            <td><a class="btn btn-danger remove">
                                                    <li class="glyphicon glyphicon-remove"></li>
                                                </a></td>
                                        </tr>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td></td>
                                        </tr>
                                    </tfoot>
                                </table>

                            </div>
                            <div class="panel panel-footer">
                                <h2>Especificaciones</h2>
                                <table class="table table-borderd">
                                    <thead>
                                        <tr>
                                            <th>Nombre</th>
                                            <th>Dimenciones</th>
                                            <th>Valor</th>
                                            <th><a class="add"><i class="glyphicon glyphicon-plus"></i></a></th>
                                        </tr>
                                    </thead>
                                    <tbody id="e">
                                        <tr>
                                            <td><input type="text" name="specification[]" class="form-control specification"></td>
                                            <td><input type="text" name="dimensions[]" class="form-control dimensions"></td>
                                            <td><input type="text" onkeypress="return filterFloat(event,this);" name="valor[]" class="form-control valor"></td>
                                            <td><a class="btn btn-danger removeTwo">
                                                    <li class="glyphicon glyphicon-remove"></li>
                                                </a></td>
                                        </tr>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td></td>
                                        </tr>
                                    </tfoot>
                                </table>

                            </div>
                            <div class="input-group control-group increment col-md-6">
                                <input type="file" name="image[]" class="form-control">
                                <div class="input-group-btn">
                                    <button class="btn btn-success" type="button"><i class="glyphicon glyphicon-plus"></i>Add</button>
                                </div>
                            </div>
                            <div class="clone hide">
                                <div class="control-group input-group col-md-6" style="margin-top:10px">
                                    <input type="file" name="image[]" class="form-control">
                                    <div class="input-group-btn">
                                        <button class="btn btn-danger" type="button"><i class="glyphicon glyphicon-remove"></i> Remove</button>
                                    </div>
                                </div>
                            </div><br>

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Guardar') }}
                                    </button>
                                </div>
                                <div class="box-footer">
                                    <a href="{{ route('products.index') }}" class="float-right btn btn-secondary">Regresar</a>
                                </div>

                            </div>
                        </form>

                    </div>

                </div>
            </div>
        </div>
    </div>
</body>
<script type="text/javascript">
    $('.addRow').on('click', function() {
        addRow();
    });

    function addRow() {
        var tr = '<tr>' +
            '<td><input type="text" name="car[]" class="form-control car"></td>' +
            '<td><input type="text" name="model_change[]" class="form-control model_change"></td>' +
            '<td><input type="text" name="motorization[]" class="form-control motorization"></td>' +
            '<td><input type="text" onkeypress="return soloNumeros(event)" name="since[]" class="form-control since"></td>' +
            '<td><input type="text" onkeypress="return soloNumeros(event)" name="until[]" class="form-control until"></td>' +
            '<td><a class="btn btn-danger remove"><li class="glyphicon glyphicon-remove"></li></a></td>' +
            '</tr>';
        $('tbody#a').append(tr);
    };
    $('.remove').live('click', function() {
        var last = $('tbody tr').length;
        if (last == 1) {
            alert("Esta fila no debe eliminar");
        } else {
            $(this).parent().parent().remove();
        }
    });
</script>

<script type="text/javascript">
    $('.add').on('click', function() {
        add();
    });

    function add() {
        var tr = '<tr>' +
            '<td><input type="text" name="specification[]" class="form-control specification"></td>' +
            '<td><input type="text" name="dimensions[]" class="form-control dimensions"></td>' +
            '<td><input type="text" onkeypress="return filterFloat(event,this);" name="valor[]" class="form-control valor"></td>' +
            '<td><a class="btn btn-danger removeTwo"><li class="glyphicon glyphicon-remove"></li></a></td>' +
            '</tr>';
        $('tbody#e').append(tr);
    };
    $('.removeTwo').live('click', function() {
        var last = $('tbody tr').length;
        if (last == 1) {
            alert("no");
        } else {
            $(this).parent().parent().remove();
        }
    });
</script>
<script>
    function soloLetras(e) {
        key = e.keyCode || e.which;
        tecla = String.fromCharCode(key).toLowerCase();
        letras = " áéíóúabcdefghijklmnñopqrstuvwxyz";
        especiales = "8-37-39-46";

        tecla_especial = false
        for (var i in especiales) {
            if (key == especiales[i]) {
                tecla_especial = true;
                break;
            }
        }

        if (letras.indexOf(tecla) == -1 && !tecla_especial) {
            return false;
        }
    }

    function soloNumeros(e) {
        key = e.keyCode || e.which;
        tecla = String.fromCharCode(key).toLowerCase();
        numeros = " 1234567890";
        especiales = [8, 37, 39, 46];

        tecla_especial = false
        for (var i in especiales) {
            if (key == especiales[i]) {
                tecla_especial = true;
                break;
            }
        }
        if (numeros.indexOf(tecla) == -1 && !tecla_especial)
            return false;
    }

    //decimales
    function filterFloat(evt, input) {
        // Backspace = 8, Enter = 13, ‘0′ = 48, ‘9′ = 57, ‘.’ = 46, ‘-’ = 43
        var key = window.Event ? evt.which : evt.keyCode;
        var chark = String.fromCharCode(key);
        var tempValue = input.value + chark;
        if (key >= 48 && key <= 57) {
            if (filter(tempValue) === false) {
                return false;
            } else {
                return true;
            }
        } else {
            if (key == 8 || key == 13 || key == 0) {
                return true;
            } else if (key == 46) {
                if (filter(tempValue) === false) {
                    return false;
                } else {
                    return true;
                }
            } else {
                return false;
            }
        }
    }

    function filter(__val__) {
        var preg = /^([0-9]+\.?[0-9]{0,2})$/;
        if (preg.test(__val__) === true) {
            return true;
        } else {
            return false;
        }

    }
</script>

<script type="text/javascript">
    $(document).ready(function() {

        $(".btn-success").click(function() {
            var html = $(".clone").html();
            $(".increment").after(html);
        });

        $("body").on("click", ".btn-danger", function() {
            $(this).parents(".control-group").remove();
        });

    });
</script>

</html>