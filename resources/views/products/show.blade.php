@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Producto</div>
                <div class="card-body">
                    <div class="row ">
                        <div class="row">
                            <div class="col-md-6">
                                <p><b>ID: </b>{{$product->id}}</p>
                                <p><b>Nombre: </b>{{$product->name}}</p>
                                <p><b>Código: </b>{{$product->code}}</p>
                                <p><b>Código Original: </b>{{$product->original_code}}</p>
                                <p><b>Precio: </b>{{$product->price}}</p>
                                <p><b>Stock: </b>{{$product->amount}}</p>
                                <p><b>Marca de Auto: </b>{{$product->car_brand}}</p>
                                <p><b>Categoría: </b>{{$product->category->category}}</p>
                            </div>
                            <div class="col-md-6">
                                <label>Marcas</label>
                                <p><b>Proveedor: </b>{{$product->brand->name}}</p>

                            </div>
                        </div>
                        <div class="row justify-content-center">
                            <div class="col-md-6">
                                <label>Aplicaciones</label>
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th scope="col">Auto</th>
                                            <th scope="col">Modelo de cambio</th>
                                            <th scope="col">Motorización</th>
                                            <th scope="col">Desde</th>
                                            <th scope="col">Hasta</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($application as $object)
                                        <tr>
                                            <td>{{$object->car}}</td>
                                            <td>{{$object->model_change}}</td>
                                            <td>{{$object->motorization}}</td>
                                            <td>{{$object->since}}</td>
                                            <td>{{$object->until}}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-md-6">
                                <label>Especificaciones</label>
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th scope="col">Especificación</th>
                                            <th scope="col">Dimención</th>
                                            <th scope="col">Valor</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($specification as $object)
                                        <tr>

                                            <td>{{$object->specification}}</td>
                                            <td>{{$object->dimensions}}</td>
                                            <td>{{$object->valor}}</td>

                                        </tr>
                                        @endforeach
                                    </tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <label><strong>Imágenes</strong></label>
                                @foreach($image as $object)
                                <img src="{{Storage::url($object->image)}}" alt="..." style="width:300px;height:400px;">
                                <!-- <p>* {{$object->image}}</p> -->
                                @endforeach
                            </div>
                        </div>
                        @include('partials.box-back')
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection