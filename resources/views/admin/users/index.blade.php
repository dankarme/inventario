@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
        <h1>
            Busqueda de usuarios
            {{ Form::open(['route' => 'admin.users.index', 'method' => 'GET', 'class' => 'form-inline pull-right']) }}
                <div class="form-group">
                    {{ Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Nombre']) }}
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-default">Buscar
                        <span class="glyphicon glyphicon-search"></span>
                    </button>
                </div>
            {{ Form::close() }}
        </h1>
        </div>
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Usuarios</div>

                <div class="card-body">
                <table class="table">
              <thead>
                <tr>
                  <th scope="col">#</th>
                  <th scope="col">Nombre</th>
                  <th scope="col">Correo Electrónico</th>
                  <th scope="col">Perfil</th>
                  <th scope="col">Acciones</th>
                </tr>
              </thead>
              <tbody>
                @foreach($users as $user)
                  <tr>
                    <th scope="row">{{$user->id}}</th>
                    <td>{{$user->name}}</td>
                    <td>{{$user->email}}</td>
                    <td>{{ implode(', ', $user->profiles()->get()->pluck('name')->toArray())}}</td>
                    <td>
                      @can('edit-users')
                      <a href="{{ route('admin.users.edit', $user->id)}}"><button type="button" class="btn btn-primary float-left">Editar</button></a>
                      @endcan
                      @can('delete-users')
                      <form action="{{ route('admin.users.destroy', $user)}}" method="POST" class="float-left">
                        @csrf
                        {{method_field('DELETE')}}
                        <button type="submit" class="btn btn-warning">Eliminar</button>
                      </form>
                      @endcan
                    </td>
                    
                  </tr>
                @endforeach
              </tbody>
            </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
