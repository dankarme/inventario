<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Route::resource('test', 'TestController');
/*Route::get('test', ['as'=>'test.index','uses'=>'TestController@index']);
Route::get('test/{id}/edit', ['as'=>'test.edit','uses'=>'TestController@edit']);
Route::get('test/{id}', ['as'=>'test.show','uses'=>'TestController@show']);
Route::delete('test/{id}/edit', ['as'=>'test.destroy','uses'=>'TestController@edit']);*/

/*Route::get('user/{id}', function($id){
  return 'Bienvenido user:'.$id;
});*/

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::namespace('Admin')->prefix('admin')->name('admin.')->middleware('can:manage-users')->group(function(){
  Route::resource('users', 'UsersController', ['except' => ['show', 'create', 'store']]);
});

Route::get('products', ['as'=>'products.index','uses'=>'ProductController@index']);
Route::get('product/create', ['as'=>'product.create','uses'=>'ProductController@create']);
Route::post('product/store', ['as' => 'product.store', 'uses' => 'ProductController@store']);
Route::get('product/edit/{id}', ['as' => 'product.edit', 'uses' => 'ProductController@edit']);
Route::patch('product/{id}', ['as' => 'product.update', 'uses' => 'ProductController@update']);
Route::delete('product/{id}', ['as' => 'product.destroy', 'uses' => 'ProductController@destroy']);
Route::get('product/{id}',['as' => 'product.show', 'uses' => 'ProductController@show']);
