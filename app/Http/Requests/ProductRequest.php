<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code' => 'required|max:10',
            'name' => 'required|max:100',
            'price' => 'max:10',
            'amount' => 'required|max:10',
            'type' => 'max:100',
            'car_brand' => 'max:100',
            'original_code' => 'required|max:255',
            'car' => 'max:255',
            'model_change' => 'max:255',
            'motorization' => 'max:255',
            'since' => 'max:255',
            'until' => 'max:255',
            'image' => 'required|array',
            'image.*' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ];
        
    }

    public function messages()
    {
        return [
            'code.required' => 'Código de producto es requerido',
            'code.max' => 'Código de producto tiene un máximo de 10 caracteres',
            'name.required' => 'Nombre de producto es requerido',
            'name.max' => 'Nombre de producto tiene demasiados caracteres',
            'price.max' => 'Precio de producto tiene demaciados caracteres',
            'amount.required'=>'Cantidad del producto es requerido',
            'amount.max'=> 'Cantidad de producto tiene demaciados caracteres',
            'type.max'=>'El tipo de producto tiene demaciados caracteres',
            'car_brand.max'=> 'Tipo de auto tiene demaciados caracteres',
            'original_code.required' => 'Código original es requerido',
            'original_code.max' => 'Código original tiene demaciados caracteres',
            'car.max' => 'El campo auto tiene demaciados caracteres',
            'model_change.max' => 'El campo modelo de cambio tiene demaciados caracteres',
            'motorization.max' => 'El campo motorización tiene demaciados caracteres',
            'since.max' => 'El campo Desde tiene demaciados caracteres',
            'until.max' => 'El campo Hasta tiene demaciados caracteres',
            'image.required' => 'Imagen es requerida'
        ];
    }
}
