<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Category;
use App\Brand;
use App\Application;
use App\Http\Requests\ProductRequest;
use App\Specification;
use App\Image;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $code = $request->get('code');
        $type = $request->get('type');
        //$brand_id = $request->get('brand_id');
        $created_at = $request->get('created_at');

        //$products = Product::with(['category','brand'])->get(); 
        $products = Product::orderBy('id', 'DESC')
            ->code($code)
            ->type($type)
            ///->brand_id($brand_id)
            ->created_at($created_at)
            ->paginate(20);
        return view('products.index')->with('products', $products);
    }






    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::orderBy('category')->get();
        $brands = Brand::orderBy('name')->get();
        return view('products.create')->with('categories', $categories)->with('brands', $brands);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    private $images ="images";

    public function store(ProductRequest $request)
    {
        try{
            $data = $request->all();
            $lastid = Product::create($data)->id;
            if (count($request->car) > 0) {
                foreach ($request->car as $application => $v) {
                    $data2 = array(
                        'car' => $request->car[$application],
                        'model_change' => $request->model_change[$application],
                        'motorization' => $request->motorization[$application],
                        'since' => $request->since[$application],
                        'until' => $request->until[$application],
                        'product_id' => $lastid
                    );
                    Application::insert($data2);
                }
            }
            if (count($request->specification) > 0) {
                foreach ($request->specification as $specification => $v) {
                    $data3 = array(
                        'specification' => $request->specification[$specification],
                        'dimensions' => $request->dimensions[$specification],
                        'valor' => $request->valor[$specification],
                        'product_id' => $lastid
                    );
                    Specification::insert($data3);
                }
            }

            //IMAGENES
            
            if($request->hasFile('image')){

                foreach($request->file('image') as $image){

                    //$path = public_path().'images';
                    //$imgName = $image->getClientOriginalName();
                    //$image->move($path,$imgName);

                    $path = $image->store($this->images);

                    Image::create([
                        'image' => $path,
                        'product_id' => $lastid
                    ]);
                }
            }


        } catch (ModelNotFoundException $exception) {
            report($exception);
            return back()->withError($exception->getMessage())->withInput();
        }

        //return $request;

        return redirect()->route('products.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         //Llama a los productos con sus categorías y marcas
         $product = Product::with(['category', 'brand'])->find($id);
       
         //Llama a las aplicaciones del producto
         $application = DB::table('applications')->where('applications.product_id','=', $id)->get();
         
         //Llama a las especificaciones del producto
         $specification = DB::table('specs')->where('specs.product_id','=', $id)->get(); 
 
         //Llama a las imagenes del producto
         $image = DB::table('images')->where('images.product_id', '=', $id)->get();
         
         return view('products.show', compact('product', 'application', 'specification','image'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $products = Product::with(['category', 'brand'])->get();
        return view('products.index')->with('products', $products);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $product = Product::find($id);
            $product->code = $request->code;
            $product->name = $request->name;
            $product->price = $request->price;
            $product->amount = $request->amount;
            $product->type = $request->type;
            $product->car_brand = $request->car_brand;
            $product->original_code = $request->original_code;
            $product->category_id = $request->category_id;
            $product->brand_id = $request->brand_id;
            $product->update();
        } catch (\Exception $e) {
            Session::flash('error', 'Ocurrió un error en el almacenamiento' . $e->getMessage());
            return back()->withInput();
        }
        Session::flash('message', 'El registro fue creado éxitosamente');
        return redirect()->route('products.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::findOrFail($id);
           //elimina a las aplicaciones del producto
           DB::table('applications')->where('applications.product_id','=', $id)->delete();
         
           //elimina a las especificaciones del producto
           DB::table('specs')->where('specs.product_id','=', $id)->delete(); 
   
           //elimina a las imagenes del producto
           DB::table('images')->where('images.product_id', '=', $id)->delete();
        
           $product->delete();

        return redirect()->route('products.index');
    }
}
