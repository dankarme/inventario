<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    /*public function users(){
      return $this->belongsToMany('App\User');
    }*/

    public function users()
    {
        return $this->belongsToMany('App\User');
    }
}
