<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $table='images';
    protected $fillable = [
        'image',
        'product_id'
    ];


    public function product(){
        return $this->belongsTo(Product::class, 'product_id');
    }

    //Con esto se puede acceder a un producto 
    //public function imageable(){
      //  return $this->morphTo();
    //}
}
