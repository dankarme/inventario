<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Specification extends Model
{
    protected $table='specs';
    protected $fillable = [
        'specification',
        'dimensions',
        'valor',
        'product_id'
    ];
    
    public function product(){
        return $this->belongsTo(Product::class, 'product_id');
    }
}
