<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Application extends Model
{
    protected $fillable = [
        'car',
        'model_change',
        'motorization',
        'since',
        'until',
        'product_id'
    ];
    
    public function product(){
        return $this->belongsTo(Product::class, 'product_id');
    }
}
