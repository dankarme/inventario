<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class Product extends Model
{
    protected $fillable = [
        'code',
        'name',
        'price',
        'amount',
        'car_brand',
        'original_code',
        'category_id',
        'brand_id'
    ];
    public function category(){
        return $this->belongsTo(Category::class , 'category_id');
    }

    public function brand(){
        return $this->belongsTo(Brand::class , 'brand_id');
    }

    public function application(){
        return $this->hasMany(Application::class);
    }

    public function specification(){
        return $this->hasMany(Specification::class);
    }

    //
    public function image(){
        return $this->hasMany(Image::class);
    }

    /*public function setImage($image, $actual = false){
         if($image){
             if($actual){
                 Storage::disk('public')->delete("images/$actual");
             }
             $imageName = $image->getClientOriginalName();
             $image = Image::make($image)->encode('jpg',75);
             $image->resize(530, 470, function ($constraint){
                 $constraint->upsize();
             });
             Storage::disk('public')->put("images/$imageName", $image->stream());
             return $imageName;
         }else{
             return false;
         }
    }*/

    public function scopeCode($query, $code)
    {
        if (trim($code) != "")
        {
            $query->orWhere('code', "LIKE", "%$code%");
        }
    }

    public function scopeType($query, $type)
    {
        if (trim($type) != "")
        {
            $query->orWhere('type', "LIKE", "%$type%");
        }
    }

    public function scopeBrand_id($query, $brand_id)
    {
        if (trim($brand_id) != "")
        {
            $query->orWhere('brand_id', "LIKE", "%$brand_id%");
        }
    }

    public function scopeCreated_at($query, $created_at)
    {
        if (trim($created_at) != "")
        {
            $query->orWhere('created_at', "LIKE", "%$created_at%");
        }
    }
}
