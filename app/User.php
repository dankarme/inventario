<?php

namespace App;

use App\Profile;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;


class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function profiles(){
      return $this->belongsToMany('App\Profile');
    }

    public function hasAnyProfiles($profiles){
        if($this->profiles()->whereIn('name', $profiles)->first()){
            return true;
        }
        return false;
    }

    public function hasProfile($profile){
        if($this->profiles()->where('name', $profile)->first()){
            return true;
        }
        return false;
    }

    public function scopeName($query, $name)
    {
        if (trim($name) != "")
        {
            $query->where('name', "LIKE", "%$name%");
        }
    }

}
