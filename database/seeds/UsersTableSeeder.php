<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Profile;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();
        DB::table('profile_user')->truncate();

        $adminProfile = Profile::where('name', 'admin')->first();
        $sellerProfile = Profile::where('name', 'seller')->first();
        $grocerProfile = Profile::where('name', 'grocer')->first();
        $userProfile = Profile::where('name', 'user')->first();

        $admin = User::create([
          'name' => 'Admin User',
          'email' => 'admin@admin.com',
          'password' => Hash::make('password')
        ]);

        $seller = User::create([
          'name' => 'Seller User',
          'email' => 'seller@seller.com',
          'password' => Hash::make('password')
        ]);

        $grocer = User::create([
          'name' => 'Grocer User',
          'email' => 'grocer@grocer.com',
          'password' => Hash::make('password')
        ]);

        $user = User::create([
          'name' => 'User',
          'email' => 'user@user.com',
          'password' => Hash::make('password')
        ]);

        $admin->profiles()->attach($adminProfile);
        $seller->profiles()->attach($sellerProfile);
        $grocer->profiles()->attach($grocerProfile);
        $user->profiles()->attach($userProfile);
    }
}
