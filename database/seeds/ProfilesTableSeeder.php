<?php

use Illuminate\Database\Seeder;
use App\Profile;

class ProfilesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Profile::truncate();

        Profile::create(['name' => 'admin']);
        Profile::create(['name' => 'seller']);
        Profile::create(['name' => 'grocer']);
        Profile::create(['name' => 'user']);
    }
}
